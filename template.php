<?php
$HEADER_TEMPLATE = "<!DOCTYPE html>
<html lang='en'>
  <head>
    <meta content='text/html; charset=utf-8' http-equiv='Content-Type' />
    <meta content='OpenNIC is a free democratic, censorship free, and community run DNS solution! Easy to setup!' />
    <title>Use OpenNIC - The Democratic, Censorship Free, DNS Solution.</title>
    <!--[if lt IE 9]>
      <script src='//html5shim.googlecode.com/svn/trunk/html5.js'></script>
    <![endif]-->
    <link href='//fonts.googleapis.com/css?family=Droid+Sans:400,700' rel='stylesheet' type='text/css' />
    <link href='css/style-default.css' media='all' rel='stylesheet' type='text/css' />
    <link href='css/style-print.css' media='print' rel='stylesheet' type='text/css' />
	  <script src='//ajax.googleapis.com/ajax/libs/jquery/1.6.4/jquery.min.js'></script>
    <script src='/js/bootstrap.min.js'></script>
	<script src='/js/bootstrap-tooltip.js'></script>
    <script src='/js/opennic.js'></script>
  </head>
  <body id='home'>
    <div class='topbar'>
      <div class='header'>
        <div class='container'>
          <a class='brand' href='/' title='Converting People To OpenNIC Since 2012'>
            <img alt='OpenNIC' height='28' src='/img/logo.png'/>
          </a>
          <h1 class='site-title'>
            <a href='/' title='The OpenNIC Awareness Project'>
              Use OpenNIC
            </a>
          </h1>
          <ul class='nav'>
            <li>
              <a class='$home' href='/'>
                Home
              </a>
            </li>
            <li>
              <a class='$setup' href='/setup.php'>
                Get Started
              </a>
            </li>
            <li>
              <a class='$connection' href='/connection.php'>
                Test Connection
              </a>
            </li>
          </ul>
        </div>
      </div>
    </div>";

$FOOTER_TEMPLATE = "      <hr />
      <blockquote class='big-quote'>
        <h2>
          Universal access to human knowledge is in our grasp, for the first time in the history of the world. This is not a bad thing.
        </h2>
        <small>
          <a href='http://www.craphound.com/' target='_blank' title='Cory Doctorow'>
            Cory Doctorow
          </a>
        </small>
      </blockquote>
      <div class='well ac'>
        <h3>
		<a href='/setup.php' title='Setup Your Computer For OpenNIC'>
            Get Started
          </a>
          or
          <a href='/connection.php' title='Test Your Connection To OpenNIC Root'>
            Test Your Connection
          </a>
        </h3>
      </div>
    </div>
    <!-- we are at the end of the DOM so we don't need to wait
    and with function() { } wrapping we keep our window context -->
    <script> setTimeout(function() { revisar() }, 0); </script>
  </body>
</html>";
