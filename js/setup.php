<?php
$setup = 'btn';
include('template.php');
echo $HEADER_TEMPLATE;

if ($_GET['ipv4'] != "" || $_GET['ipv6'] != "") {
$os = $_GET['os'];
$country = $_GET['country'];
if ($os == 'osx'){
$instructions = '
<h2>Step One</h2>
	<div class="well">
		<p>Click on your <strong>"Apple"</strong> menu and choose <strong>"System Preference"</strong>:</p>
		<img src="/img/macosx1.jpg">
	</div>
<h2>Step Two</h2>
	<div class="well">
		<p>Click on the <strong>"Network"</strong> icon:</p>
		<img src="/img/macosx2.jpg">
	</div>
<h2>Step Three</h2>
	<div class="well">
		<p>Go to the <strong>"TCP/IP"</strong> tab. If your computer is configured to use a dynamic IP address, you should see a screen like this one (notice <strong>"Using DHCP"</strong> in the drop-down box next to <strong>"Configure"</strong>):</p>
		<img src="/img/macosx3.jpg">
	</div>
<h2>Step Four</h2>
	<div class="well">
		<p>This is where you can change your DNS settings, click the "+" arrow at the bottom and enter any two or three of the following IPs into the <strong>"DNS Servers"</strong> box:</p>
		<div class="alert-message info"><code>10.10.10.10</code> - City, Country [ Logging Information ]</div>
		<div class="alert-message info"><code>10.10.10.10</code> - City, Country [ Logging Information ]</div>
		<div class="alert-message info"><code>10.10.10.10</code> - City, Country [ Logging Information ]</div>
		<div class="alert-message info"><code>10.10.10.10</code> - City, Country [ Logging Information ]</div>
		
		<p>
Apply your settings by clicking the <strong>"Apply Now"</strong> or <strong>"OK"</strong> button and Done!:</p>
		<img src="/img/macosx4.jpg">
	</div>
<h2>Step Five</h2>
	<div class="well">
		<p>You are done! Let\'s test your connection.</p>
	</div>
	<a href="connection.php" class="btn huge success" style="float:right;">Check Your Connection</a>
';
}
?>
<div class="hero">
      <div class="container">
        <h1 style='text-align:center;line-height:65px;'>Step By Step Instructions</h1>
      </div>
    </div>
<div class="container content">
<?php echo $instructions; ?>
</div>
<?php
} else {
function getOS($userAgent) {
  // Create list of operating systems with operating system name as array key 
	$oses = array (
		'iPhone' => '(iPhone)',
		'Windows 3.11' => 'Win16',
		'Windows 95' => '(Windows 95)|(Win95)|(Windows_95)', // Use regular expressions as value to identify operating system
		'Windows 98' => '(Windows 98)|(Win98)',
		'Windows 2000' => '(Windows NT 5.0)|(Windows 2000)',
		'Windows XP' => '(Windows NT 5.1)|(Windows XP)',
		'Windows 2003' => '(Windows NT 5.2)',
		'Windows Vista' => '(Windows NT 6.0)|(Windows Vista)',
		'Windows 7' => '(Windows NT 6.1)|(Windows 7)',
		'Windows NT 4.0' => '(Windows NT 4.0)|(WinNT4.0)|(WinNT)|(Windows NT)',
		'Windows ME' => 'Windows ME',
		'Open BSD'=>'OpenBSD',
		'Sun OS'=>'SunOS',
		'Linux'=>'(Linux)|(X11)',
		'Safari' => '(Safari)',
		'Macintosh'=>'(Mac_PowerPC)|(Macintosh)',
		'QNX'=>'QNX',
		'BeOS'=>'BeOS',
		'OS/2'=>'OS/2',
		'Search Bot'=>'(nuhk)|(Googlebot)|(Yammybot)|(Openbot)|(Slurp/cat)|(msnbot)|(ia_archiver)'
	);

	foreach($oses as $os=>$pattern){ // Loop through $oses array
    // Use regular expressions to check operating system type
		if(eregi($pattern, $userAgent)) { // Check if a value in $oses array matches current user agent.
			return $os; // Operating system was matched so return $oses key
		}
	}
	return 'Unknown'; // Cannot find operating system so return Unknown
}




?> 
<form action='setup.php'>
<div class="hero">
      <div class="container">
        <h1>Setup Your Computer</h1>
      </div>
    </div>
<div class="container content">
	<div class="well">
		<h2>What Country To Host Your DNS?<small>Choose the closest country to you (usually)</small></h2>
		<p>Your country was matched as: <strong>
			<?php
			$IP = $_SERVER['REMOTE_ADDR'];
			$SSID = htmlentities(SID);
			if (!empty($IP)) {
			  $country=file_get_contents('http://api.hostip.info/get_html.php?ip='.$IP);
			list ($_country) = explode ("\n", $country);
			$_country = str_replace("Country: ", "", $_country);
			echo $_country;
			}
			?>
		</strong></p>
		<div class="control-group">
            <label class="control-label" for="country">Country </label>
            <div class="controls">
              <select id="country" name="country">
                <option>Please Select</option>
                <option>United States</option>
                <option>Canada</option>
                <option>Germany</option>
                <option>United Kingdom</option>
              </select>
            </div>
          </div>
	</div>
	<hr/>
	<div class="well">
		<h2>What Operating System Do You Run?</h2>
		<p>Your Operating System was matched as: <strong><?php echo getOS($_SERVER['HTTP_USER_AGENT']); ?></strong></p>
		<div class="control-group">
            <label class="control-label" for="os">Operating System </label>
            <div class="controls">
              <select id="os" name="os">
                <option>Please Select</option>
                <option value='xp'>Windows XP</option>
                <option value='vista'>Windows Vista</option>
                <option value='win7'>Windows 7</option>
				<option value='osx'>MacOS X</option>
				<option value='ubuntu114'>Ubuntu 11.04</option>
				<option value='linux'>Other Linux</option>
              </select>
            </div>
          </div>
	</div>
	<hr/>
	<button class='btn huge success' style="float:right" type="submit" name="ipv4" value="true">Lets Do It!</button>
	 <button class='btn huge info' style="float:right" type="submit" name="ipv6" value="true">IPv6</button>
</form>
</div>
<?php
}
?>