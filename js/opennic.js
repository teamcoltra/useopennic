/**
 * Use OpenNIC
 * User: nanashiRei
 * Date: 06.12.12
 * Time: 05:12
 *TODO: Change description
 */


function revisar() {
    /**IMAGENES VALIDOS
     * @var {Object} images */
    var images = {
        cacheBuster:(new Date().getTime()),
        error:'http://maximi89.linuxerz.org/wp-content/uploads/2011/07/opennic_invalido.png',
        cargando:'http://maximi89.linuxerz.org/wp-content/uploads/2011/07/opennic_flor.gif',

        bbs:'http://www.opennictest.bbs/opennic_valido.png?' + this.cacheBuster,
        //!gopher: 'http://www.opennictest.gopher/opennic_valido.png?'+ this.cacheBuster,
        gopher:'http://pongonova.gopher/gopherwiki/images/opennic_valido.png?' + this.cacheBuster,
        fur:'http://www.opennictest.fur/opennic_valido.png?' + this.cacheBuster,
        free:'http://www.opennictest.free/opennic_valido.png?' + this.cacheBuster,
        geek:'http://www.opennictest.geek/opennic_valido.png?' + this.cacheBuster,
        indy:'http://www.opennictest.indy/opennic_valido.png?' + this.cacheBuster,
        null:'http://www.opennictest.null/opennic_valido.png?' + this.cacheBuster,
        //!oss: 'http://www.opennictest.oss/opennic_valido.png?'+ this.cacheBuster,
        oss:'http://opennic.oss/files/opennic_valido.png?' + this.cacheBuster,
        //!parody: 'http://www.opennictest.parody/opennic_valido.png?'+ this.cacheBuster,
        parody:'http://opennic.parody/files/opennic_valido.png?' + this.cacheBuster,
        micro:'http://www.opennictest.micro/opennic_valido.png?' + this.cacheBuster,
        ing:'http://www.opennictest.ing/opennic_valido.png?' + this.cacheBuster,
        dyn:'http://www.opennictest.dyn/opennic_valido.png?' + this.cacheBuster,
        p2p:'http://www.opennictest.p2p/opennic_valido.png?' + this.cacheBuster,
        bit:'http://www.opennictest.bit/opennic_valido.png?' + this.cacheBuster,
        bzh:'http://www.opennictest.bzh/opennic_valido.png?' + this.cacheBuster,

        ///////IMAGENES VALIDOS IPV6/////////
        bbs_ipv:'http://ipv6.opennictest.bbs/opennic_valido.png',
        gopher_ipv:'http://ipv6.opennictest.gopher/opennic_valido.png',
        fur_ipv:'http://ipv6.opennictest.fur/opennic_valido.png',
        free_ipv:'http://ipv6.opennictest.free/opennic_valido.png',
        geek_ipv:'http://ipv6.opennictest.geek/opennic_valido.png',
        indy_ipv:'http://ipv6.opennictest.indy/opennic_valido.png',
        null_ipv:'http://ipv6.opennictest.null/opennic_valido.png',
        oss_ipv:'http://ipv6.opennictest.oss/opennic_valido.png',
        parody_ipv:'http://ipv6.opennictest.parody/opennic_valido.png',
        micro_ipv:'http://ipv6.opennictest.micro/opennic_valido.png',
        ing_ipv:'http://ipv6.opennictest.ing/opennic_valido.png',
        dyn_ipv:'http://ipv6.opennictest.dyn/opennic_valido.png',
        p2p_ipv:'http://ipv6.opennictest.p2p/opennic_valido.png',
        bit_ipv:'http://ipv6.opennictest.bit/opennic_valido.png',
        bzh_ipv:'http://ipv6.opennictest.bzh/opennic_valido.png',
    };

    ///////IMAGENES CARGANDO DURANTE LA REVISION////////////////
    document.getElementById('imagen_bbs').src = images.cargando;
    document.getElementById('imagen_gopher').src = images.cargando;
    document.getElementById('imagen_fur').src = images.cargando;
    document.getElementById('imagen_free').src = images.cargando;
    document.getElementById('imagen_geek').src = images.cargando;
    document.getElementById('imagen_indy').src = images.cargando;
    document.getElementById('imagen_null').src = images.cargando;
    document.getElementById('imagen_oss').src = images.cargando;
    document.getElementById('imagen_parody').src = images.cargando;
    document.getElementById('imagen_micro').src = images.cargando;
    document.getElementById('imagen_ing').src = images.cargando;

    //
    // Just trying to show you how you can make this file about 70% smaller :P
    //

    /** chainable onload */
    Image.prototype.setOnLoad = function (eventHandler) {
        this.onload = eventHandler;
        return this;
    };
    /** chainable onerror */
    Image.prototype.setOnError = function (eventHandler) {
        this.onerror = eventHandler;
        return this;
    };

    /**
     * If you ever what to change how the images load / behave you will love for this :P
     * @param {string} id
     * @param {string} img
     * @param {string} color
     * @param {string} bbqvar
     */
    var onLoadTransform = function (id, img, color, bbqvar) {
        document.getElementById(id).src = img;
        if (!bbqvar) {
            bbqvar = 'titulo';
        }
        document.getElementById(bbqvar).style.color = color;
    };

    var onErrorTransform = function (id) {
        document.getElementById(id).src = images.error;
    };

    /** don't repeat yourself, your are scripting :P */
    var imageLoadList = [
        {id:'imagen_bbs', img:images.bbs, errColor:'#33FF00'},
        {id:'imagen_bbs_ipv', img:images.bbs_ipv, errColor:'#33FF00'},
        {id:'imagen_free', img:images.free, errColor:'#33FF00'},
        {id:'imagen_free_ipv', img:images.free_ipv, errColor:'#33FF00'},
        {id:'imagen_fur', img:images.fur, errColor:'#33FF00'},
        {id:'imagen_fur_ipv', img:images.fur_ipv, errColor:'#33FF00'},
        {id:'imagen_geek', img:images.geek, errColor:'#33FF00'},
        {id:'imagen_geek_ipv', img:images.geek_ipv, errColor:'#33FF00'},
        {id:'imagen_gopher', img:images.gopher, errColor:'#33FF00'},
        {id:'imagen_gopher_ipv', img:images.gopher_ipv, errColor:'#33FF00'},
        {id:'imagen_indy', img:images.indy, errColor:'#33FF00'},
        {id:'imagen_indy_ipv', img:images.indy_ipv, errColor:'#33FF00'},
        {id:'imagen_ing', img:images.ing, errColor:'#33FF00'},
        {id:'imagen_ing_ipv', img:images.ing_ipv, errColor:'#33FF00'},
        {id:'imagen_null', img:images.null, errColor:'#33FF00'},
        {id:'imagen_null_ipv', img:images.null_ipv, errColor:'#33FF00'},
        {id:'imagen_oss', img:images.oss, errColor:'#33FF00'},
        {id:'imagen_oss_ipv', img:images.oss_ipv, errColor:'#33FF00'},
        {id:'imagen_micro', img:images.micro, errColor:'#33FF00'},
        {id:'imagen_micro_ipv', img:images.micro_ipv, errColor:'#33FF00'},
        {id:'imagen_parody', img:images.parody, errColor:'#33FF00'},
        {id:'imagen_parody_ipv', img:images.parody_ipv, errColor:'#33FF00'}
    ];

    //
    // You need this loop exactly once, so this is it, for all
    // of your images!!!
    //
    imageLoadList.forEach(function (imageTag) {
        new Image()
            .setOnLoad(function () {
                onLoadTransform(imageTag.id, imageTag.img, imageTag.errColor);
            })
            .setOnError(function () {
                onErrorTransform('imagen_bbs');
            })
            .src = imageTag.img;
    });

    //
    // End of my demo
    //

}